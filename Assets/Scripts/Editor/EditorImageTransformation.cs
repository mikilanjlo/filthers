﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ImageTransformation))]
public class EditorImageTransformation : Editor
{
    
    

    public override void OnInspectorGUI()
    {
        ImageTransformation image = target as ImageTransformation;

       image._computeShader = (ComputeShader)EditorGUILayout.ObjectField("Compute Shader", image._computeShader, typeof(ComputeShader), false);

        if (image.spriteRenderer == null)
            image.spriteRenderer = image.GetComponent<SpriteRenderer>();
        if (image.sprite == null)
            image.spriteRenderer.sprite = null;

        image.sprite = (Sprite)EditorGUILayout.ObjectField("Icon", image.sprite, typeof(Sprite), false);

        image.filter = (ImageTransformation.Filters)EditorGUILayout.EnumPopup("Filter", image.filter);

        switch (image.filter)
        {
            case ImageTransformation.Filters.BoxBlur:
                EditorGUI.BeginChangeCheck();
                image.windowSize = EditorGUILayout.IntSlider("Window size", image.windowSize, 0 ,30);
                if (EditorGUI.EndChangeCheck())
                {
                    image.spriteRenderer.sprite = image.ToBlur(image.sprite, image.windowSize);
                }
                break;
            case ImageTransformation.Filters.Previtta:
            case ImageTransformation.Filters.Robertsa:
            case ImageTransformation.Filters.Sobol:
                image.brightness = EditorGUILayout.Toggle("Brightness", image.brightness);
                break;
            case ImageTransformation.Filters.Median:
            case ImageTransformation.Filters.Median2:
                EditorGUI.BeginChangeCheck();
                image.windowSize = EditorGUILayout.IntSlider("Window size", image.windowSize, 0, 20);
                image.medianK = EditorGUILayout.IntSlider("Median coefficient", image.medianK, 0, (image.windowSize * image.windowSize + 1) / 2);
                if (EditorGUI.EndChangeCheck())
                {
                    //image.spriteRenderer.sprite = image.ToBlur(image.sprite, image.windowSize);
                }
                break;
            case ImageTransformation.Filters.Gauss:
                EditorGUI.BeginChangeCheck();
                image.windowSize = EditorGUILayout.IntSlider("Window size", image.windowSize, 0, 20);
                image.sigma = EditorGUILayout.Slider("Sigma", image.sigma, 0, 20);
                if (EditorGUI.EndChangeCheck())
                {
                    //image.spriteRenderer.sprite = image.ToBlur(image.sprite, image.windowSize);
                }
                break;
        }

        if (GUILayout.Button("Set Origin"))
        {
            image.spriteRenderer.sprite = image.sprite;
        }
        if (GUILayout.Button("Filter"))
        {
            switch (image.filter)
            {
                case ImageTransformation.Filters.BoxBlur:
                    image.spriteRenderer.sprite = image.ToBlur(image.sprite, image.windowSize);
                    break;
                case ImageTransformation.Filters.Sobol:
                    image.spriteRenderer.sprite = image.SobolFilther(image.sprite, image.brightness);
                    break;
                case ImageTransformation.Filters.Previtta:
                    image.spriteRenderer.sprite = image.PrevittaFilther(image.sprite, image.brightness);
                    break;
                case ImageTransformation.Filters.Robertsa:
                    image.spriteRenderer.sprite = image.RobertsaFilther(image.sprite, image.brightness);
                    break;
                case ImageTransformation.Filters.Median:
                    image.spriteRenderer.sprite = image.MedianFilther(image.sprite, image.windowSize,image.medianK);
                    break;
                case ImageTransformation.Filters.Median2:
                    image.spriteRenderer.sprite = image.Median2Filther(image.sprite, image.windowSize, image.medianK);
                    break;
                case ImageTransformation.Filters.Gauss:
                    image.spriteRenderer.sprite = image.Gauss(image.sprite, image.windowSize, image.sigma);
                    break;
            }
            
        }
        if (GUILayout.Button("Save"))
        {
            byte[] bytes = image.spriteRenderer.sprite.texture.EncodeToPNG();
            var path = EditorUtility.SaveFilePanel("Save Image", "Assets", "image", "png");
            if (path.Length == 0) return;
            File.WriteAllBytes(path, bytes);
        }


    }
}
